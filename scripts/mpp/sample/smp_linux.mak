
# target source
OBJS  := $(SMP_SRCS:%.c=%.o)

CFLAGS += $(COMM_INC)

MPI_LIBS += $(REL_LIB)/libss_hdmi.a
MPI_LIBS += $(LIBS_LD_CFLAGS)

.PHONY: all clean copy_other

all:$(TARGET) copy_other

$(TARGET): $(COMM_OBJ) $(OBJS)
	mkdir -p $(TARGET_PATH)
	$(CC) $(CFLAGS) -lpthread -lm -o $(TARGET_PATH)/$@ $^ -Wl,--start-group $(MPI_LIBS) $(SENSOR_LIBS) $(AUDIO_LIBA)  $(INIPARSER_LIB)  $(REL_LIB)/libsecurec.a -Wl,--end-group

%.o: %.c
	$(CC) $(CFLAGS) -c $< -o $@

clean:
	rm -rf $(TARGET_PATH)
	rm -f $(OBJS)
	rm -f $(COMM_OBJ)

cleanstream:
	@rm -f *.h264
	@rm -f *.h265
	@rm -f *.jpg
	@rm -f *.mjp
	@rm -f *.mp4

copy_other:
	@if [ -d "$(SRCFILE_DIR)" ]; then \
		mkdir -p $(TARGET_PATH);\
		cp -r $(SRCFILE_DIR) $(TARGET_PATH)/; \
	fi