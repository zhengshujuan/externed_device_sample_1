#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <unistd.h>
#include <termios.h>

void send_data(int fd, const char *data) {
    int bytes_written = write(fd, data, strlen(data));
    if (bytes_written == -1) {
        perror("写入串口数据时发生错误");
    } else {
        printf("成功写入 %d 字节的数据: %s\n", bytes_written, data);
    }
}

int main(int argc, char *argv[]) {
    if (argc != 4) {
        fprintf(stderr, "用法: %s <串口设备路径> <波特率> <要发送的数据>\n", argv[0]);
        return 1;
    }

    const char *serial_port = argv[1];
    int baud_rate = atoi(argv[2]);
    const char *data_to_send = argv[3];

    int fd = open(serial_port, O_RDWR | O_NOCTTY);
    if (fd == -1) {
        perror("无法打开串口");
        return 1;
    }

    struct termios options;
    if(tcgetattr(fd, &options) != 0) {
        perror("Error from tcgetattr");
        close(fd);
        return -1;
    }

    // 设置波特率
    cfsetispeed(&options, baud_rate);
    cfsetospeed(&options, baud_rate);

    // 设置串口参数
    options.c_cflag |= (CLOCAL | CREAD);
    options.c_cflag &= ~PARENB;
    options.c_cflag &= ~CSTOPB;
    options.c_cflag &= ~CSIZE;
    options.c_cflag |= CS8;

    options.c_iflag &= ~(INPCK | ISTRIP);
    options.c_lflag &= ~(ICANON | ECHO | ECHOE | ISIG);
    options.c_oflag &= ~OPOST;

    tcflush(fd, TCIFLUSH);
    // 应用设置
    if (tcsetattr(fd, TCSANOW, &options) != 0) {
        perror("Error from tcsetattr");
        close(fd);
        return -1;
    }

    send_data(fd, data_to_send);

    close(fd);

    return 0;
}
