#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <unistd.h>
#include <termios.h>

#define BUFFER_SIZE 256

void printUsage() {
    printf("Usage: ./serial_receiver <serial_port> <baud_rate>\n");
}

int main(int argc, char *argv[]) {
    if (argc != 3) {
        printUsage();
        return -1;
    }

    char *serial_port = argv[1];
    int baud_rate = atoi(argv[2]);
    int serial_fd;
    char buffer[BUFFER_SIZE];

    // 打开串口
    serial_fd = open(serial_port, O_RDWR);
    if (serial_fd == -1) {
        perror("Error opening serial port");
        return -1;
    }

    // 配置串口波特率
    struct termios tty;
    memset(&tty, 0, sizeof(tty));
    if (tcgetattr(serial_fd, &tty) != 0) {
        perror("Error from tcgetattr");
        close(serial_fd);
        return -1;
    }

    cfsetospeed(&tty, (speed_t)baud_rate);
    cfsetispeed(&tty, (speed_t)baud_rate);

    // 设置其他串口参数
    tty.c_cflag |= CREAD | CLOCAL; // 使能接收和忽略调制解调器线路状态
    tty.c_cflag &= ~CSIZE;          // 屏蔽数据位
    tty.c_cflag |= CS8;             // 8位数据位
    tty.c_cflag &= ~PARENB;         // 禁用奇偶校验
    tty.c_cflag &= ~CSTOPB;         // 1位停止位
    tty.c_cflag &= ~CRTSCTS;        // 禁用硬件流控制

    // 设置输入模式
    tty.c_iflag &= ~(IXON | IXOFF | IXANY); // 禁用软件流控制

    // 设置输出模式
    tty.c_oflag &= ~OPOST; // 原始输出

    // 设置本地模式
    tty.c_lflag = 0; // 非规范模式

    // 设置超时参数
    tty.c_cc[VTIME] = 0;    // 读取一个字符的超时时间
    tty.c_cc[VMIN] = 1;     // 阻塞读取的最小字符数

    // 设置属性
    if (tcsetattr(serial_fd, TCSANOW, &tty) != 0) {
        perror("Error from tcsetattr");
        close(serial_fd);
        return -1;
    }

    // 读取数据
    while (1) {
        memset(buffer, 0, sizeof(buffer));
        int bytesRead = read(serial_fd, buffer, sizeof(buffer) - 1);
        if (bytesRead > 0) {
            printf("Received data: %s\n", buffer);
        } else if (bytesRead < 0) {
            perror("Error reading from serial port");
            break;
        }
        usleep(100000);  // 等待100ms
    }

    // 关闭串口
    close(serial_fd);

    return 0;
}
