# CAN

## 一 例程简介

CAN例程为tof距离数据接收

## 二 测试环境说明

### 1)硬件准备

- 欧拉派
- TOF

### 2）软件环境

无

### 三 硬件连接

如图所示为CAN信号的接线端子，CAN H L信号的顺序为左L 右H

![image-20240201144537828](./README.assets/image-20240201144537828.png)

与TOF的连接如图所示，TOF的H线和欧拉派的CAN_H对应，TOF的L线和欧拉派的CAN_L对应，TOF的GND连接在欧拉派40Pin排针的GND上，TOF的5v供电连接在欧拉派40pin排针的5V上

![image-20240201144951481](./README.assets/image-20240201144951481.png)

## 四 例程运行说明

在开始测试前需要将can网卡节点开启，在欧拉派的终端上执行如下命令

```c
ip link set can0 type can bitrate 500000
ip link set can0 up
```

__TOF数据接收测试__

使用 can_tof命令进行接收测试，在欧拉派的终端上执行

```C
./can_tof
```

应用开启后持续接收TOF发送的距离数据并打印到欧拉派终端，使用ctrl+c结束接收





