/*
  Copyright (c), 2001-2022, Shenshu Tech. Co., Ltd.
  h265->vdec->vpss->vo->mipi_tx
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <sys/poll.h>
#include <sys/time.h>
#include <fcntl.h>
#include <errno.h>
#include <pthread.h>
#include <math.h>
#include <unistd.h>
#include <signal.h>

#include "ot_mipi_tx.h"
#include "sample_comm.h"

#define SAMPLE_VO_DEV_FRAME_RATE 60
#define SAMPLE_STREAM_PATH "./source_file"
#define UHD_STREAN_WIDTH 1920
#define UHD_STREAM_HEIGHT 1200

#define REF_NUM 5
#define DISPLAY_NUM 2
#define SAMPLE_VDEC_COMM_VB_CNT 4
#define SAMPLE_VDEC_VPSS_LOW_DELAY_LINE_CNT 16

static ot_payload_type g_cur_type = OT_PT_H265;

static vdec_display_cfg g_vdec_display_cfg = {
    .pic_size = PIC_1920X1200,
    .intf_sync = OT_VO_OUT_USER,
    .intf_type = OT_VO_INTF_MIPI,
};

char g_file_name[1024];

#define USLEEP_6000   6000
#define USLEEP_50     50
#define USLEEP_100000 100000
#define USLEEP_60000  60000
#define CMD_COUNT_1200X1920 304
static mipi_tx_cmd_info g_cmd_info_1200x1920[CMD_COUNT_1200X1920] = {
    /* {devno work_mode lp_clk_en data_type cmd_size cmd}, usleep_value */
    {{0, 0, 0, 0x23, 0x00B0, NULL}, USLEEP_6000},
    {{0, 0, 0, 0x23, 0x05B0, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0xE5B1, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0x52B3, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0x05C0, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0x85D9, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0x55C2, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0x00B0, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0x88B3, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0x0BB6, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0x8BBA, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0x1ABF, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0x0FC0, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0x0CC2, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0x02C3, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0x0CC4, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0x02C5, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0x01B0, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0x26E0, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0x26E1, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0x00DC, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0x00DD, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0x26CC, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0x26CD, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0x00C8, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0x00C9, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0x03D2, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0x03D3, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0x04E6, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0x04E7, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0x09C4, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0x09C5, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0x0AD8, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0x0AD9, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0x0BC2, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0x0BC3, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0x0CD6, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0x0CD7, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0x05C0, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0x05C1, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0x06D4, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0x06D5, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0x07CA, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0x07CB, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0x08DE, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0x08DF, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0x02B0, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0x00C0, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0x0FC1, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0x1AC2, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0x2BC3, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0x38C4, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0x39C5, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0x38C6, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0x38C7, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0x36C8, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0x34C9, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0x35CA, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0x36CB, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0x39CC, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0x2DCD, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0x2DCE, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0x2CCF, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0x07D0, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0x00D2, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0x0FD3, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0x1AD4, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0x2BD5, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0x38D6, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0x39D7, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0x38D8, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0x38D9, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0x36DA, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0x34DB, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0x35DC, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0x36DD, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0x39DE, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0x2DDF, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0x2DE0, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0x2CE1, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0x07E2, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0x03B0, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0x0BC8, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0x07C9, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0x00C3, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0x00E7, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0x2AC5, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0x2ADE, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0x43CA, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0x07C9, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0xC0E4, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0x0DE5, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0x00CB, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0x06B0, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0xA5B8, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0xA5C0, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0x0FC7, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0x32D5, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0x00B8, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0x00C0, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0x00BC, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0x07B0, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0x08B1, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0x09B2, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0x13B3, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0x23B4, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0x37B5, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0x4BB6, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0x6DB7, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0x9CB8, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0xD8B9, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0x17BA, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0x93BB, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0x1BBC, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0x1FBD, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0x9BBE, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0x19BF, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0x57C0, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0x93C1, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0xAEC2, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0xCAC3, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0xD7C4, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0xE5C5, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0xF3C6, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0xF9C7, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0xFCC8, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0x00C9, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0x00CA, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0x16CB, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0xAFCC, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0xFFCD, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0xFFCE, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0x08B0, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0x04B1, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0x04B2, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0x10B3, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0x23B4, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0x37B5, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0x4BB6, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0x6DB7, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0x9CB8, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0xD8B9, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0x17BA, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0x93BB, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0x1CBC, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0x20BD, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0x9DBE, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0x1CBF, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0x5BC0, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0x96C1, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0xB1C2, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0xCDC3, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0xDAC4, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0xE7C5, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0xF4C6, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0xFAC7, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0xFCC8, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0x00C9, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0x00CA, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0x16CB, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0xAFCC, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0xFFCD, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0xFFCE, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0x09B0, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0x04B1, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0x04B2, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0x0DB3, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0x22B4, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0x37B5, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0x4CB6, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0x6EB7, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0x9DB8, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0xDAB9, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0x19BA, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0x9ABB, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0x25BC, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0x29BD, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0xA7BE, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0x26BF, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0x63C0, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0x9CC1, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0xB6C2, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0xD1C3, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0xDCC4, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0xE9C5, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0xF5C6, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0xFAC7, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0xFCC8, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0x00C9, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0x00CA, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0x16CB, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0xAFCC, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0xFFCD, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0xFFCE, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0x0AB0, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0x08B1, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0x09B2, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0x13B3, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0x23B4, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0x37B5, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0x4BB6, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0x6DB7, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0x9CB8, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0xD8B9, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0x17BA, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0x93BB, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0x1BBC, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0x1FBD, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0x9BBE, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0x19BF, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0x57C0, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0x93C1, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0xAEC2, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0xCAC3, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0xD7C4, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0xE5C5, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0xF3C6, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0xF9C7, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0xFCC8, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0x00C9, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0x00CA, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0x16CB, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0xAFCC, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0xFFCD, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0xFFCE, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0x0BB0, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0x04B1, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0x04B2, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0x10B3, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0x23B4, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0x37B5, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0x4BB6, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0x6DB7, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0x9CB8, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0xD8B9, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0x17BA, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0x93BB, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0x1CBC, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0x20BD, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0x9DBE, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0x1CBF, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0x5BC0, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0x96C1, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0xB1C2, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0xCDC3, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0xDAC4, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0xE7C5, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0xF4C6, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0xFAC7, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0xFCC8, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0x00C9, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0x00CA, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0x16CB, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0xAFCC, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0xFFCD, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0xFFCE, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0x0CB0, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0x04B1, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0x04B2, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0x0DB3, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0x22B4, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0x37B5, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0x4CB6, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0x6EB7, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0x9DB8, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0xDAB9, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0x19BA, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0x9ABB, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0x25BC, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0x29BD, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0xA7BE, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0x26BF, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0x63C0, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0x9CC1, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0xB6C2, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0xD1C3, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0xDCC4, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0xE9C5, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0xF5C6, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0xFAC7, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0xFCC8, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0x00C9, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0x00CA, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0x16CB, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0xAFCC, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0xFFCD, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0xFFCE, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0x00B0, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0x08B3, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0x04B0, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0xFFB8, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0x02B5, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0x01B6, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0x6FB1, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0xC8F0, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0xD6F1, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0xE0F2, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0xF2F3, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0xFBF4, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0xFDF5, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0xFDF6, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0xFEF7, NULL}, USLEEP_50},
    {{0, 0, 0, 0x23, 0x0011, NULL}, USLEEP_100000},
    {{0, 0, 0, 0x23, 0x0029, NULL}, USLEEP_60000},
};

/* VO: USER 1200x1920_60, TX: USER 1200x1920 */
static const sample_vo_mipi_tx_cfg g_vo_tx_cfg_1200x1920_user = {
    .vo_config = {
        .vo_dev = SAMPLE_VO_DEV_UHD,
        .vo_intf_type = OT_VO_INTF_MIPI,
        .intf_sync = OT_VO_OUT_USER,
        .bg_color = COLOR_RGB_BLACK,
        .pix_format = OT_PIXEL_FORMAT_YVU_SEMIPLANAR_420,
        .disp_rect = {0, 0, 1200, 1920},
        .image_size = {1200, 1920},
        .vo_part_mode = OT_VO_PARTITION_MODE_SINGLE,
        .dis_buf_len = 3, /* 3: def buf len for single */
        .dst_dynamic_range = OT_DYNAMIC_RANGE_SDR8,
        .vo_mode = VO_MODE_4MUX,
        .compress_mode = OT_COMPRESS_MODE_NONE,

        .sync_info = {0, 1, 1, 1920, 12, 15, 1200, 104, 60, 1, 540, 9, 7, 24, 2, 0, 0, 0},
        .user_sync = {
            .user_sync_attr = {
                .clk_src = OT_VO_CLK_SRC_PLL,
                .vo_pll = { /* if hdmi, set it by pixel clk and div mode */
                    .fb_div = 80, /* 80 fb div */
                    .frac = 0,
                    .ref_div = 1, /* 1 ref div */
                    .post_div1 = 6, /* 6 post div1 */
                    .post_div2 = 2, /* 2 post div2 */
                },
            },
            .pre_div = 1, /* if hdmi, set it by pixel clk */
            .dev_div = 1, /* if rgb, set it by serial mode */
            .clk_reverse_en = TD_FALSE,
        },
        .dev_frame_rate = SAMPLE_VO_DEV_FRAME_RATE,
    },
    .tx_config = {
        /* for combo dev config */
        .intf_sync = OT_MIPI_TX_OUT_USER,

        /* for screen cmd */
        .cmd_count = 0,
        .cmd_info = NULL,

        /* for user sync */
        .combo_dev_cfg = {
            .devno = 0,
            .lane_id = {0, 1, 2, 3},
            .out_mode = OUT_MODE_DSI_VIDEO,
            .out_format = OUT_FORMAT_RGB_24BIT,
            .video_mode =  BURST_MODE,
            .sync_info = {
                .hsa_pixels = 24, /* 24 pixel */
                .hbp_pixels = 80, /* 80 pixel */
                .hact_pixels = 1200, /* 1200 pixel */
                .hfp_pixels = 60, /* 60 pixel */
                .vsa_lines = 2, /* 2 line */
                .vbp_lines = 10, /* 10 line */
                .vact_lines = 1920, /* 1920 line */
                .vfp_lines = 15, /* 15 line */
            },
            .phy_data_rate = 999, /* 999 Mbps */
            .pixel_clk = 160000, /* 160000 KHz */
        },
    },
};

static ot_size g_disp_size;
static td_s32 g_sample_exit = 0;

static td_void sample_vdec_handle_sig(td_s32 signo)
{
    if ((signo == SIGINT) || (signo == SIGTERM)) {
        g_sample_exit = 1;
    }
}

static td_s32 sample_getchar()
{
    int c;
    if (g_sample_exit == 1) {
        return 'e';
    }

    c = getchar();

    if (g_sample_exit == 1) {
        return 'e';
    }
    return c;
}

static td_u32 sample_vdec_get_chn_width()
{
    switch (g_cur_type) {
        case OT_PT_H264:
        case OT_PT_H265:
            return UHD_STREAN_WIDTH;
        case OT_PT_JPEG:
        case OT_PT_MJPEG:
            return UHD_STREAN_WIDTH;
        default:
            sample_print("invalid type %d!\n", g_cur_type);
            return UHD_STREAN_WIDTH;
    }
}

static td_u32 sample_vdec_get_chn_height()
{
    switch (g_cur_type) {
        case OT_PT_H264:
        case OT_PT_H265:
            return UHD_STREAM_HEIGHT;
        case OT_PT_JPEG:
        case OT_PT_MJPEG:
            return UHD_STREAM_HEIGHT;
        default:
            sample_print("invalid type %d!\n", g_cur_type);
            return UHD_STREAM_HEIGHT;
    }
}

static td_s32 sample_init_module_vb(sample_vdec_attr *sample_vdec, td_u32 vdec_chn_num, ot_payload_type type,
    td_u32 len)
{
    td_u32 i;
    td_s32 ret;
    for (i = 0; (i < vdec_chn_num) && (i < len); i++) {
        sample_vdec[i].type                           = type;
        sample_vdec[i].width                         = sample_vdec_get_chn_width(type);
        sample_vdec[i].height                        = sample_vdec_get_chn_height(type);
        sample_vdec[i].mode                           = sample_comm_vdec_get_lowdelay_en() ? OT_VDEC_SEND_MODE_COMPAT :
                                                        OT_VDEC_SEND_MODE_FRAME;
        sample_vdec[i].sample_vdec_video.dec_mode      = OT_VIDEO_DEC_MODE_IP;
        sample_vdec[i].sample_vdec_video.bit_width     = OT_DATA_BIT_WIDTH_8;
        if (type == OT_PT_JPEG) {
            sample_vdec[i].sample_vdec_video.ref_frame_num = 0;
        } else {
            sample_vdec[i].sample_vdec_video.ref_frame_num = REF_NUM;
        }
        sample_vdec[i].display_frame_num               = DISPLAY_NUM;
        sample_vdec[i].frame_buf_cnt = (type == OT_PT_JPEG) ? (sample_vdec[i].display_frame_num + 1) :
            (sample_vdec[i].sample_vdec_video.ref_frame_num + sample_vdec[i].display_frame_num + 1);
        if (type == OT_PT_JPEG) {
            sample_vdec[i].sample_vdec_picture.pixel_format = OT_PIXEL_FORMAT_YVU_SEMIPLANAR_420;
            sample_vdec[i].sample_vdec_picture.alpha      = 255; /* 255:pic alpha value */
        }
    }
    ret = sample_comm_vdec_init_vb_pool(vdec_chn_num, &sample_vdec[0], len);
    if (ret != TD_SUCCESS) {
        sample_print("init mod common vb fail for %#x!\n", ret);
        return ret;
    }
    return ret;
}

static td_s32 sample_init_sys_and_vb(sample_vdec_attr *sample_vdec, td_u32 vdec_chn_num, ot_payload_type type,
    td_u32 len)
{
    ot_vb_cfg vb_cfg;
    ot_pic_buf_attr buf_attr = {0};
    td_s32 ret;

    ret = sample_comm_sys_get_pic_size(g_vdec_display_cfg.pic_size, &g_disp_size);
    if (ret != TD_SUCCESS) {
        sample_print("sys get pic size fail for %#x!\n", ret);
        return ret;
    }
    buf_attr.align = OT_DEFAULT_ALIGN;
    buf_attr.bit_width = OT_DATA_BIT_WIDTH_8;
    buf_attr.compress_mode = OT_COMPRESS_MODE_SEG;
    buf_attr.height = g_disp_size.height;
    buf_attr.width = g_disp_size.width;
    buf_attr.pixel_format = OT_PIXEL_FORMAT_YVU_SEMIPLANAR_420;

    (td_void)memset_s(&vb_cfg, sizeof(ot_vb_cfg), 0, sizeof(ot_vb_cfg));
    vb_cfg.max_pool_cnt             = 1;
    vb_cfg.common_pool[0].blk_cnt  = SAMPLE_VDEC_COMM_VB_CNT * vdec_chn_num;
    vb_cfg.common_pool[0].blk_size = ot_common_get_pic_buf_size(&buf_attr);
    ret = sample_comm_sys_init(&vb_cfg);
    if (ret != TD_SUCCESS) {
        sample_print("init sys fail for %#x!\n", ret);
        sample_comm_sys_exit();
        return ret;
    }
    ret = sample_init_module_vb(&sample_vdec[0], vdec_chn_num, type, len);
    if (ret != TD_SUCCESS) {
        sample_print("init mod vb fail for %#x!\n", ret);
        sample_comm_vdec_exit_vb_pool();
        sample_comm_sys_exit();
        return ret;
    }
    return ret;
}

static td_s32 sample_vdec_bind_vpss(td_u32 vpss_grp_num)
{
    td_u32 i;
    td_s32 ret = TD_SUCCESS;
    for (i = 0; i < vpss_grp_num; i++) {
        ret = sample_comm_vdec_bind_vpss(i, i);
        if (ret != TD_SUCCESS) {
            sample_print("vdec bind vpss fail for %#x!\n", ret);
            return ret;
        }
    }
    return ret;
}

static td_void sample_stop_vpss(ot_vpss_grp vpss_grp, td_bool *vpss_chn_enable, td_u32 chn_array_size)
{
    td_s32 i;
    for (i = vpss_grp; i >= 0; i--) {
        vpss_grp = i;
        sample_common_vpss_stop(vpss_grp, &vpss_chn_enable[0], chn_array_size);
    }
}

static td_s32 sample_vdec_unbind_vpss(td_u32 vpss_grp_num)
{
    td_u32 i;
    td_s32 ret = TD_SUCCESS;
    for (i = 0; i < vpss_grp_num; i++) {
        ret = sample_comm_vdec_un_bind_vpss(i, i);
        if (ret != TD_SUCCESS) {
            sample_print("vdec unbind vpss fail for %#x!\n", ret);
        }
    }
    return ret;
}

static td_void sample_config_vpss_grp_attr(ot_vpss_grp_attr *vpss_grp_attr)
{
    vpss_grp_attr->max_width = sample_vdec_get_chn_width();
    vpss_grp_attr->max_height = sample_vdec_get_chn_height();
    vpss_grp_attr->frame_rate.src_frame_rate = -1;
    vpss_grp_attr->frame_rate.dst_frame_rate = -1;
    vpss_grp_attr->pixel_format  = OT_PIXEL_FORMAT_YVU_SEMIPLANAR_420;
    vpss_grp_attr->nr_en   = TD_FALSE;
    vpss_grp_attr->ie_en   = TD_FALSE;
    vpss_grp_attr->dci_en   = TD_FALSE;
    vpss_grp_attr->dei_mode = OT_VPSS_DEI_MODE_OFF;
    vpss_grp_attr->buf_share_en   = TD_FALSE;
}

static td_s32 sample_config_vpss_ldy_attr(td_u32 vpss_grp_num)
{
    td_u32 i;
    td_s32 ret;
    ot_low_delay_info vpss_ldy_info;
    if (!sample_comm_vdec_get_lowdelay_en()) {
        return TD_SUCCESS;
    }
    for (i = 0; i < vpss_grp_num; i++) {
        ret = ss_mpi_vpss_get_low_delay_attr(i, 0, &vpss_ldy_info);
        if (ret != TD_SUCCESS) {
            sample_print("vpss get low delay attr fail for %#x!\n", ret);
            return ret;
        }
        vpss_ldy_info.enable = TD_TRUE;
        vpss_ldy_info.line_cnt = SAMPLE_VDEC_VPSS_LOW_DELAY_LINE_CNT;
        ret = ss_mpi_vpss_set_low_delay_attr(i, 0, &vpss_ldy_info);
        if (ret != TD_SUCCESS) {
            sample_print("vpss set low delay attr fail for %#x!\n", ret);
            return ret;
        }
    }
    return TD_SUCCESS;
}

static td_s32 sample_start_vpss(ot_vpss_grp *vpss_grp, td_u32 vpss_grp_num, td_bool *vpss_chn_enable, td_u32 arr_len)
{
    td_u32 i;
    td_s32 ret;
    ot_vpss_chn_attr vpss_chn_attr[OT_VPSS_MAX_CHN_NUM];
    ot_vpss_grp_attr vpss_grp_attr = {0};
    sample_config_vpss_grp_attr(&vpss_grp_attr);
    (td_void)memset_s(vpss_chn_enable, arr_len * sizeof(td_bool), 0, arr_len * sizeof(td_bool));

    vpss_chn_enable[0] = TD_TRUE;
    vpss_chn_attr[0].width         = g_disp_size.width; /* 4:crop */
    vpss_chn_attr[0].height        = g_disp_size.height; /* 4:crop */
    vpss_chn_attr[0].compress_mode = OT_COMPRESS_MODE_SEG;
    vpss_chn_attr[0].chn_mode                  = OT_VPSS_CHN_MODE_USER;
    vpss_chn_attr[0].pixel_format              = OT_PIXEL_FORMAT_YVU_SEMIPLANAR_420;
    vpss_chn_attr[0].frame_rate.src_frame_rate = -1;
    vpss_chn_attr[0].frame_rate.dst_frame_rate = -1;
    vpss_chn_attr[0].depth                     = 0;
    vpss_chn_attr[0].mirror_en                 = TD_FALSE;
    vpss_chn_attr[0].flip_en                   = TD_FALSE;
    vpss_chn_attr[0].border_en                 = TD_FALSE;
    vpss_chn_attr[0].aspect_ratio.mode         = OT_ASPECT_RATIO_NONE;

    for (i = 0; i < vpss_grp_num; i++) {
        *vpss_grp = i;
        ret = sample_common_vpss_start(*vpss_grp, &vpss_chn_enable[0],
            &vpss_grp_attr, vpss_chn_attr, OT_VPSS_MAX_CHN_NUM);
        if (ret != TD_SUCCESS) {
            sample_print("start VPSS fail for %#x!\n", ret);
            sample_stop_vpss(*vpss_grp, &vpss_chn_enable[0], OT_VPSS_MAX_CHN_NUM);
            return ret;
        }
    }

    ret = sample_config_vpss_ldy_attr(vpss_grp_num);
    if (ret != TD_SUCCESS) {
        sample_stop_vpss(*vpss_grp, &vpss_chn_enable[0], OT_VPSS_MAX_CHN_NUM);
        return ret;
    }

    ret = sample_vdec_bind_vpss(vpss_grp_num);
    if (ret != TD_SUCCESS) {
        sample_vdec_unbind_vpss(vpss_grp_num);
        sample_stop_vpss(*vpss_grp, &vpss_chn_enable[0], OT_VPSS_MAX_CHN_NUM);
    }
    return ret;
}

static td_s32 sample_vpss_unbind_vo(td_u32 vpss_grp_num, sample_vo_cfg vo_config)
{
    td_u32 i;
    ot_vo_layer vo_layer = vo_config.vo_dev;
    td_s32 ret = TD_SUCCESS;
    for (i = 0; i < vpss_grp_num; i++) {
        ret = sample_comm_vpss_un_bind_vo(i, 0, vo_layer, i);
        if (ret != TD_SUCCESS) {
            sample_print("vpss unbind vo fail for %#x!\n", ret);
        }
    }
    return ret;
}

static td_s32 sample_vpss_bind_vo(sample_vo_cfg vo_config, td_u32 vpss_grp_num)
{
    td_u32 i;
    ot_vo_layer vo_layer;
    td_s32 ret = TD_SUCCESS;
    vo_layer = vo_config.vo_dev;
    for (i = 0; i < vpss_grp_num; i++) {
        ret = sample_comm_vpss_bind_vo(i, 0, vo_layer, i);
        if (ret != TD_SUCCESS) {
            sample_print("vpss bind vo fail for %#x!\n", ret);
            return ret;
        }
    }
    return ret;
}

static td_s32 start_vo_mipi_tx(const sample_vo_mipi_tx_cfg *vo_tx_cfg, td_u32 vpss_grp_num)
{
    td_s32 ret;
    const sample_vo_cfg *vo_config = &vo_tx_cfg->vo_config;
    const sample_mipi_tx_config *tx_config = &vo_tx_cfg->tx_config;

    ret = sample_comm_vo_start_vo(vo_config);
    if (ret != TD_SUCCESS) {
        sample_print("start vo failed with 0x%x!\n", ret);
        return ret;
    }
    printf("start vo dhd%d.\n", vo_config->vo_dev);

    ret = sample_vpss_bind_vo(*vo_config, vpss_grp_num);
    if (ret != TD_SUCCESS) {
        sample_vpss_unbind_vo(vpss_grp_num, *vo_config);
        sample_comm_vo_stop_vo(vo_config);
    }

    if ((vo_config->vo_intf_type & OT_VO_INTF_MIPI) ||
        (vo_config->vo_intf_type & OT_VO_INTF_MIPI_SLAVE)) {
        ret = sample_comm_start_mipi_tx(tx_config);
        if (ret != TD_SUCCESS) {
            sample_print("start mipi tx failed with 0x%x!\n", ret);
            return ret;
        }
    }

    return TD_SUCCESS;
}

static void sample_full_chn(td_u32 chn_index)
{
    td_s32 ret;
    ot_vo_chn_attr chn_attr;
    chn_attr.priority = 0;
    chn_attr.deflicker_en = TD_FALSE;
    chn_attr.rect.x = 0;
    chn_attr.rect.y = 0;
    printf("sample_set_vo_chn_attr chn = %d\n", chn_index);
    for (td_u32 i = 0; i < 4; i++) {
        if (i == chn_index) {
            chn_attr.rect.width = g_vo_tx_cfg_1200x1920_user.vo_config.image_size.width;
            chn_attr.rect.height = g_vo_tx_cfg_1200x1920_user.vo_config.image_size.height;
            ret = ss_mpi_vo_set_chn_attr(0, chn_index, &chn_attr);
            if (ret != TD_SUCCESS) {
                sample_print("ss_mpi_vo_set_chn_attr failed with %#x!\n", ret);
            }
            ret = ss_mpi_vo_enable_chn(0, i);
            if (ret != TD_SUCCESS) {
                sample_print("ss_mpi_vo_enable_chn failed with %#x!\n", ret);
            }
        } else {
            ret = ss_mpi_vo_disable_chn(0, i);
            if (ret != TD_SUCCESS) {
                sample_print("ss_mpi_vo_disable_chn failed with %#x!\n", ret);
            }
        }
    }
}

static void sample_restore_chn()
{
    td_s32 ret;
    ot_vo_chn_attr chn_attr;
    chn_attr.priority = 0;
    chn_attr.deflicker_en = TD_FALSE;
    for (td_u32 i = 0; i < 4; i++) {
        chn_attr.rect.x = OT_ALIGN_DOWN((g_vo_tx_cfg_1200x1920_user.vo_config.image_size.width / 2) * (i % 2), 2);
        chn_attr.rect.y = OT_ALIGN_DOWN((g_vo_tx_cfg_1200x1920_user.vo_config.image_size.height / 2) * (i / 2), 2);
        chn_attr.rect.width = OT_ALIGN_DOWN(g_vo_tx_cfg_1200x1920_user.vo_config.image_size.width / 2, 2);
        chn_attr.rect.height = OT_ALIGN_DOWN(g_vo_tx_cfg_1200x1920_user.vo_config.image_size.height / 2, 2);
        ret = ss_mpi_vo_set_chn_attr(0, i, &chn_attr);
        if (ret != TD_SUCCESS) {
            sample_print("ss_mpi_vo_set_chn_attr failed with %#x!\n", ret);
        }
        ret = ss_mpi_vo_enable_chn(0, i);
        if (ret != TD_SUCCESS) {
            sample_print("ss_mpi_vo_enable_chn failed with %#x!\n", ret);
        }
    }
}

static td_void sample_vdec_cmd_ctrl(td_u32 chn_num, vdec_thread_param *vdec_send, pthread_t *vdec_thread,
    td_u32 send_arr_len, td_u32 thread_arr_len)
{
    td_u32 i;
    ot_vdec_chn_status status;
    td_s32 c;

    for (i = 0; (i < chn_num) && (i < send_arr_len) && (i < thread_arr_len); i++) {
        if (vdec_send[i].circle_send == TD_TRUE) {
            goto circle_send;
        }
    }

    sample_comm_vdec_cmd_not_circle_send(chn_num, vdec_send, vdec_thread, send_arr_len, thread_arr_len);
    return;

circle_send:
    while (1) {
        printf("\n_sample_test:press 'e' to exit; 'q' to query!;\n");
        c = sample_getchar();
        if (c == 'e') {
            break;
        } else if (c >= '0' && c <= '3') {
                sample_full_chn(c - 48);
        } else if (c == 'r') {
                sample_restore_chn();
        } else if (c == 'q') {
            for (i = 0; (i < chn_num) && (i < send_arr_len) && (i < thread_arr_len); i++) {
                ss_mpi_vdec_query_status(vdec_send[i].chn_id, &status);
                sample_comm_vdec_print_chn_status(vdec_send[i].chn_id, status);
            }
        }
    }
    return;
}

static td_void sample_send_stream_to_vdec(sample_vdec_attr *sample_vdec, td_u32 arr_len, td_u32 vdec_chn_num)
{
    td_u32 i;
    vdec_thread_param vdec_send[OT_VDEC_MAX_CHN_NUM];
    pthread_t   vdec_thread[OT_VDEC_MAX_CHN_NUM] = {0}; /* 2:thread */
    if (arr_len > OT_VDEC_MAX_CHN_NUM) {
        sample_print("array size(%u) of vdec_send need < %u!\n", arr_len, OT_VDEC_MAX_CHN_NUM);
        return;
    }
    for (i = 0; (i < vdec_chn_num) && (i < arr_len); i++) {
        if (snprintf_s(vdec_send[i].c_file_name, sizeof(vdec_send[i].c_file_name), sizeof(vdec_send[i].c_file_name) - 1,
            g_file_name) < 0) {
            return;
        }
        if (snprintf_s(vdec_send[i].c_file_path, sizeof(vdec_send[i].c_file_path), sizeof(vdec_send[i].c_file_path) - 1,
            "%s", SAMPLE_STREAM_PATH) < 0) {
            return;
        }
        vdec_send[i].type          = sample_vdec[i].type;
        vdec_send[i].stream_mode   = sample_vdec[i].mode;
        vdec_send[i].chn_id        = i;
        vdec_send[i].interval_time = 1000; /* 1000: interval time */
        vdec_send[i].pts_init      = 0;
        vdec_send[i].pts_increase  = 0;
        vdec_send[i].e_thread_ctrl = THREAD_CTRL_START;
        vdec_send[i].circle_send   = TD_TRUE;
        vdec_send[i].milli_sec     = 0;
        vdec_send[i].min_buf_size  = (sample_vdec[i].width * sample_vdec[i].height * 3) >> 1; /* 3:yuv */
        vdec_send[i].fps           = 30; /* 30:frame rate */
    }
    sample_comm_vdec_start_send_stream(vdec_chn_num, &vdec_send[0], &vdec_thread[0],
        OT_VDEC_MAX_CHN_NUM, OT_VDEC_MAX_CHN_NUM);

    sample_vdec_cmd_ctrl(vdec_chn_num, &vdec_send[0], &vdec_thread[0],
        OT_VDEC_MAX_CHN_NUM, OT_VDEC_MAX_CHN_NUM);

    sample_comm_vdec_stop_send_stream(vdec_chn_num, &vdec_send[0], &vdec_thread[0],
        OT_VDEC_MAX_CHN_NUM, OT_VDEC_MAX_CHN_NUM);
}

static td_s32 sample_start_vdec(sample_vdec_attr *sample_vdec, td_u32 vdec_chn_num, td_u32 len)
{
    td_s32 ret;

    ret = sample_comm_vdec_start(vdec_chn_num, &sample_vdec[0], len);
    if (ret != TD_SUCCESS) {
        sample_print("start VDEC fail for %#x!\n", ret);
        sample_comm_vdec_stop(vdec_chn_num);
    }

    return ret;
}

static td_s32 sample_h265_vdec_vpss_vo(td_void)
{
    td_s32 ret;
    td_u32 vdec_chn_num, vpss_grp_num;
    sample_vdec_attr sample_vdec[OT_VDEC_MAX_CHN_NUM];
    td_bool vpss_chn_enable[OT_VPSS_MAX_CHN_NUM];
    ot_vpss_grp vpss_grp;

    vdec_chn_num = 4;
    vpss_grp_num = vdec_chn_num;
    g_cur_type = OT_PT_H265;
    /************************************************
    step1:  init SYS, init common VB(for VPSS and VO), init module VB(for VDEC)
    *************************************************/
    ret = sample_init_sys_and_vb(&sample_vdec[0], vdec_chn_num, g_cur_type, OT_VDEC_MAX_CHN_NUM);
    if (ret != TD_SUCCESS) {
        return ret;
    }
    /************************************************
    step2:  init VDEC
    *************************************************/
    ret = sample_start_vdec(&sample_vdec[0], vdec_chn_num, OT_VDEC_MAX_CHN_NUM);
    if (ret != TD_SUCCESS) {
        goto stop_sys;
    }
    /************************************************
    step3:  start VPSS
    *************************************************/
    ret = sample_start_vpss(&vpss_grp, vpss_grp_num, &vpss_chn_enable[0], OT_VPSS_MAX_CHN_NUM);
    if (ret != TD_SUCCESS) {
        goto stop_vdec;
    }
    /************************************************
    step4:  start VO
    *************************************************/
    ret = start_vo_mipi_tx(&g_vo_tx_cfg_1200x1920_user, vpss_grp_num);
    if (ret != TD_SUCCESS) {
        goto stop_vpss;
    }

    /************************************************
    step5:  send stream to VDEC
    *************************************************/
    sample_send_stream_to_vdec(&sample_vdec[0], OT_VDEC_MAX_CHN_NUM, vdec_chn_num);

    ret = sample_vpss_unbind_vo(vpss_grp_num, g_vo_tx_cfg_1200x1920_user.vo_config);
    sample_comm_stop_mipi_tx(g_vo_tx_cfg_1200x1920_user.vo_config.vo_intf_type);
    sample_comm_vo_stop_vo(&g_vo_tx_cfg_1200x1920_user.vo_config);
stop_vpss:
    ret = sample_vdec_unbind_vpss(vpss_grp_num);
    sample_stop_vpss(vpss_grp, &vpss_chn_enable[0], OT_VPSS_MAX_CHN_NUM);
stop_vdec:
    sample_comm_vdec_stop(vdec_chn_num);
    sample_comm_vdec_exit_vb_pool();
stop_sys:
    sample_comm_sys_exit();

    return ret;
}

int main(int argc, char *argv[])
{
    td_s32 ret;
    if (argc != 2) {
        printf("sample_vdec file_name\n");
        return -1;
    }
    strcpy(g_file_name, argv[1]);

    sample_sys_signal(sample_vdec_handle_sig);

    ret = sample_h265_vdec_vpss_vo();
    if (ret == TD_SUCCESS && g_sample_exit == 0) {
        sample_print("program exit normally!\n");
    } else {
        sample_print("program exit abnormally!\n");
    }

    return ret;
}
