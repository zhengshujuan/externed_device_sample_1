/**
 * Copyright (c) @CompanyNameMagicTag 2022. All rights reserved.
 *
 * Description: SLE private service register sample of client.
 */
#include "securec.h"
#include "sle_device_discovery.h"
#include "sle_connection_manager.h"
#include "sle_ssap_client.h"
#include "sle_uuid_client.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <unistd.h>
#include <termios.h>
#include <sys/stat.h>
#include <signal.h>

#undef THIS_FILE_ID
#define THIS_FILE_ID BTH_GLE_SAMPLE_UUID_CLIENT
#define SLE_RCU_DONGLE_LOG                          "[sle ws73]"
#define SLE_MTU_SIZE_DEFAULT        300
#define SLE_SEEK_INTERVAL_DEFAULT   100
#define SLE_SEEK_WINDOW_DEFAULT     100
#define UUID_16BIT_LEN 2
#define UUID_128BIT_LEN 16
char key_code = 0;
char key_value = 0;

sle_announce_seek_callbacks_t g_seek_cbk = {0};
sle_connection_callbacks_t    g_connect_cbk = {0};
ssapc_callbacks_t             g_ssapc_cbk = {0};
sle_addr_t                    g_remote_addr = {0};
uint16_t                      g_conn_id = 0;
ssapc_find_service_result_t   g_find_service_result = {0};

#define test_suite_uart_sendf(fmt, arg...) NULL
#define CONFIG_SLE_MULTICON_CLIENT_ADDR0            0xb0
#define CONFIG_SLE_MULTICON_CLIENT_ADDR1            0x66
#define CONFIG_SLE_MULTICON_CLIENT_ADDR2            0x00
#define CONFIG_SLE_MULTICON_CLIENT_ADDR3            0x00
#define CONFIG_SLE_MULTICON_CLIENT_ADDR4            0x7A
#define CONFIG_SLE_MULTICON_CLIENT_ADDR5            0x00

#define USB_RCU_KEYBOARD_REPORTER_LEN       9
#define USB_RCU_MOUSE_REPORTER_LEN          5
#define USB_RCU_CONSUMER_REPORTER_LEN       3
#define USB_HID_RCU_MAX_KEY_LENTH      6
typedef struct usb_hid_rcu_keyboard_report {
    uint8_t kind;
    uint8_t special_key;                         /*!< 8bit special key(Lctrl Lshift Lalt Lgui Rctrl Rshift Ralt Rgui) */
    uint8_t reserve;
    uint8_t key[USB_HID_RCU_MAX_KEY_LENTH]; /*!< Normal key */
} usb_hid_rcu_keyboard_report_t;

void send_data(int fd, char *data) {
    int bytes_written = write(fd, data, strlen(data));
    if (bytes_written == -1) {
        perror("Error occurred while writing data to the serial port");
    } else {
        printf("Write %d bytes of data success : %s\n", bytes_written, data);
    }
}

int send_data_to_serial(char *data)
{
	char *serial_port = "/dev/ttyAMA3";
    speed_t baud_rate = B9600;

	int fd = open(serial_port, O_RDWR | O_NOCTTY);
    if (fd == -1) {
        perror("Open serial port fail\n");
        return -1;
    }

    struct termios options;
    if(tcgetattr(fd, &options) != 0) {
        perror("Error from tcgetattr");
        close(fd);
        return -1;
    }

    // Set baud rate
    cfsetispeed(&options, baud_rate);
    cfsetospeed(&options, baud_rate);

    // Set serial port parameters
    options.c_cflag |= (CLOCAL | CREAD);
    options.c_cflag &= ~PARENB;
    options.c_cflag &= ~CSTOPB;
    options.c_cflag &= ~CSIZE;
    options.c_cflag |= CS8;

    options.c_iflag &= ~(INPCK | ISTRIP);
    options.c_lflag &= ~(ICANON | ECHO | ECHOE | ISIG);
    options.c_oflag &= ~OPOST;

    tcflush(fd, TCIFLUSH);
    // Apply settings
    if (tcsetattr(fd, TCSANOW, &options) != 0) {
        perror("Error from tcsetattr");
        close(fd);
        return -1;
    }
	send_data(fd, data);

    close(fd);
	return 0;
}

void sle_sample_sle_enable_cbk(errcode_t status)
{
	errcode_t ret;
	uint8_t local_addr[SLE_ADDR_LEN] = { CONFIG_SLE_MULTICON_CLIENT_ADDR0, CONFIG_SLE_MULTICON_CLIENT_ADDR1,
                                         CONFIG_SLE_MULTICON_CLIENT_ADDR2, CONFIG_SLE_MULTICON_CLIENT_ADDR3,
                                         CONFIG_SLE_MULTICON_CLIENT_ADDR4, CONFIG_SLE_MULTICON_CLIENT_ADDR5 };
    sle_addr_t local_address;
    local_address.type = 0;
    memcpy_s(local_address.addr, SLE_ADDR_LEN, local_addr, SLE_ADDR_LEN);
    if (status == 0) {
		ret = sle_set_local_addr(&local_address);
		printf("sle_set_local_addr %d\n",ret);
        sle_start_scan();
    }
}

void sle_sample_seek_enable_cbk(errcode_t status)
{
    if (status == 0) {
        return;
    }
}

void sle_sample_seek_disable_cbk(errcode_t status)
{
    if (status == 0) {
        sle_connect_remote_device(&g_remote_addr);
    }
}

void sle_sample_seek_result_info_cbk(sle_seek_result_info_t *seek_result_data)
{
    if (seek_result_data != NULL) {
        (void)memcpy_s(&g_remote_addr, sizeof(sle_addr_t), &seek_result_data->addr, sizeof(sle_addr_t));
        sle_stop_seek();
    }
}

void sle_sample_seek_cbk_register(void)
{
    g_seek_cbk.sle_enable_cb = sle_sample_sle_enable_cbk;
    g_seek_cbk.seek_enable_cb = sle_sample_seek_enable_cbk;
    g_seek_cbk.seek_disable_cb = sle_sample_seek_disable_cbk;
    g_seek_cbk.seek_result_cb = sle_sample_seek_result_info_cbk;
}

void sle_sample_connect_state_changed_cbk(uint16_t conn_id, const sle_addr_t *addr,
    sle_acb_state_t conn_state, sle_pair_state_t pair_state, sle_disc_reason_t disc_reason)
{
    test_suite_uart_sendf("[ssap client] conn state changed conn_id:%d, addr:%02x***%02x%02x\n", conn_id, addr->addr[0],
        addr->addr[4], addr->addr[5]); /* 0 4 5: addr index */
    test_suite_uart_sendf("[ssap client] conn state changed disc_reason:0x%x\n", disc_reason);
    if (conn_state == SLE_ACB_STATE_CONNECTED) {
        if (pair_state == SLE_PAIR_NONE) {
            sle_pair_remote_device(&g_remote_addr);
        }
        g_conn_id = conn_id;
    }
}

void sle_sample_pair_complete_cbk(uint16_t conn_id, const sle_addr_t *addr, errcode_t status)
{
    test_suite_uart_sendf("[ssap client] pair complete conn_id:%d, addr:%02x***%02x%02x\n", conn_id, addr->addr[0],
        addr->addr[4], addr->addr[5]); /* 0 4 5: addr index */
    if (status == 0) {
        ssap_exchange_info_t info = {0};
        info.mtu_size = SLE_MTU_SIZE_DEFAULT;
        info.version = 1;
        ssapc_exchange_info_req(1, g_conn_id, &info);
    }
}

void sle_sample_connect_cbk_register(void)
{
    g_connect_cbk.connect_state_changed_cb = sle_sample_connect_state_changed_cbk;
    g_connect_cbk.pair_complete_cb = sle_sample_pair_complete_cbk;
}

void sle_sample_exchange_info_cbk(uint8_t client_id, uint16_t conn_id, ssap_exchange_info_t *param,
    errcode_t status)
{
    test_suite_uart_sendf("[ssap client] pair complete client id:%d status:%d\n", client_id, status);
    test_suite_uart_sendf("[ssap client] exchange mtu, mtu size: %d, version: %d.\n",
        param->mtu_size, param->version);

    ssapc_find_structure_param_t find_param = {0};
    find_param.type = SSAP_FIND_TYPE_PRIMARY_SERVICE;
    find_param.start_hdl = 1;
    find_param.end_hdl = 0xFFFF;
    ssapc_find_structure(0, conn_id, &find_param);
}

void sle_sample_find_structure_cbk(uint8_t client_id, uint16_t conn_id, ssapc_find_service_result_t *service,
    errcode_t status)
{
    test_suite_uart_sendf("[ssap client] find structure cbk client: %d conn_id:%d status: %d \n",
        client_id, conn_id, status);
    test_suite_uart_sendf("[ssap client] find structure start_hdl:[0x%02x], end_hdl:[0x%02x], uuid len:%d\r\n",
        service->start_hdl, service->end_hdl, service->uuid.len);
    if (service->uuid.len == UUID_16BIT_LEN) {
        test_suite_uart_sendf("[ssap client] structure uuid:[0x%02x][0x%02x]\r\n",
            service->uuid.uuid[14], service->uuid.uuid[15]); /* 14 15: uuid index */
    } else {
        for (uint8_t idx = 0; idx < UUID_128BIT_LEN; idx++) {
            test_suite_uart_sendf("[ssap client] structure uuid[%d]:[0x%02x]\r\n", idx, service->uuid.uuid[idx]);
        }
    }
    g_find_service_result.start_hdl = service->start_hdl;
    g_find_service_result.end_hdl = service->end_hdl;
    memcpy_s(&g_find_service_result.uuid, sizeof(sle_uuid_t), &service->uuid, sizeof(sle_uuid_t));
}

void sle_sample_find_structure_cmp_cbk(uint8_t client_id, uint16_t conn_id,
    ssapc_find_structure_result_t *structure_result, errcode_t status)
{
    test_suite_uart_sendf("[ssap client] find structure cmp cbk client id:%d status:%d type:%d uuid len:%d \r\n",
        client_id, status, structure_result->type, structure_result->uuid.len);
    if (structure_result->uuid.len == UUID_16BIT_LEN) {
        test_suite_uart_sendf("[ssap client] find structure cmp cbk structure uuid:[0x%02x][0x%02x]\r\n",
            structure_result->uuid.uuid[14], structure_result->uuid.uuid[15]); /* 14 15: uuid index */
    } else {
        for (uint8_t idx = 0; idx < UUID_128BIT_LEN; idx++) {
            test_suite_uart_sendf("[ssap client] find structure cmp cbk structure uuid[%d]:[0x%02x]\r\n", idx,
                structure_result->uuid.uuid[idx]);
        }
    }
    uint8_t data[] = {0x11, 0x22, 0x33, 0x44};
    uint8_t len = sizeof(data);
    ssapc_write_param_t param = {0};
    param.handle = g_find_service_result.start_hdl;
    param.type = SSAP_PROPERTY_TYPE_VALUE;
    param.data_len = len;
    param.data = data;
    ssapc_write_req(0, conn_id, &param);
}

void sle_sample_find_property_cbk(uint8_t client_id, uint16_t conn_id,
    ssapc_find_property_result_t *property, errcode_t status)
{
    test_suite_uart_sendf("[ssap client] find property cbk, client id: %d, conn id: %d, operate ind: %d, "
        "descriptors count: %d status:%d.\n", client_id, conn_id, property->operate_indication,
        property->descriptors_count, status);
    for (uint16_t idx = 0; idx < property->descriptors_count; idx++) {
        test_suite_uart_sendf("[ssap client] find property cbk, descriptors type [%d]: 0x%02x.\n",
            idx, property->descriptors_type[idx]);
    }
    if (property->uuid.len == UUID_16BIT_LEN) {
        test_suite_uart_sendf("[ssap client] find property cbk, uuid: %02x %02x.\n",
            property->uuid.uuid[14], property->uuid.uuid[15]); /* 14 15: uuid index */
    } else if (property->uuid.len == UUID_128BIT_LEN) {
        for (uint16_t idx = 0; idx < UUID_128BIT_LEN; idx++) {
            test_suite_uart_sendf("[ssap client] find property cbk, uuid [%d]: %02x.\n",
                idx, property->uuid.uuid[idx]);
        }
    }
}

void sle_sample_write_cfm_cbk(uint8_t client_id, uint16_t conn_id, ssapc_write_result_t *write_result,
    errcode_t status)
{
    test_suite_uart_sendf("[ssap client] write cfm cbk, client id: %d status:%d.\n", client_id, status);
    ssapc_read_req(0, conn_id, write_result->handle, write_result->type);
}

void sle_sample_read_cfm_cbk(uint8_t client_id, uint16_t conn_id, ssapc_handle_value_t *read_data,
    errcode_t status)
{
    test_suite_uart_sendf("[ssap client] read cfm cbk client id: %d conn id: %d status: %d\n",
        client_id, conn_id, status);
    test_suite_uart_sendf("[ssap client] read cfm cbk handle: %d, type: %d , len: %d\n",
        read_data->handle, read_data->type, read_data->data_len);
    for (uint16_t idx = 0; idx < read_data->data_len; idx++) {
        test_suite_uart_sendf("[ssap client] read cfm cbk[%d] 0x%02x\r\n", idx, read_data->data[idx]);
    }
}

char key_code_switch(char key_code)
{
	switch(key_code){
	case 5:
		key_code = 48;
		break;
	case 7:
		key_code = 32;
		break;
	case 82:
		key_code = 103;
		break;
	case 80:
		key_code = 105;
		break;
	case 40:
		key_code = 28;
		break;
	case 79:
		key_code = 106;
		break;
	case 81:
		key_code = 108;
		break;
	case 41:
		key_code = 1;
		break;
	case 101:
		key_code = 127;
		break;
	case 74:
		key_code = 102;
		break;
	case 30:
	case 31:
	case 32:
	case 33:
	case 34:
	case 35:
	case 36:
	case 37:
	case 38:
	case 39:
	case 42:
		key_code = key_code - 28;
		break;
	default:
		break;
	}
	return key_code;
}

static void sle_rcu_notification_cb(uint8_t client_id, uint16_t conn_id, ssapc_handle_value_t *data, errcode_t status)
{
	char data_to_send[30];
    if (data == NULL || data->data_len == 0 || data->data == NULL) {
        printf("%s sle_rcu_notification_cb fail, recv data is null!\r\n", SLE_RCU_DONGLE_LOG);
    }
	printf("%s sle_rcu_notification_cb ok\n", SLE_RCU_DONGLE_LOG);
    if (data->data_len == USB_RCU_KEYBOARD_REPORTER_LEN) {
        usb_hid_rcu_keyboard_report_t *recv_usb_hid_rcu_keyboard = NULL;
        printf("%s sle rcu recive notification\r\n", SLE_RCU_DONGLE_LOG);
        recv_usb_hid_rcu_keyboard = (usb_hid_rcu_keyboard_report_t *)data->data;
        printf("%s recv_usb_hid_rcu_keyboard.kind = [%d]\r\n", SLE_RCU_DONGLE_LOG,
                    recv_usb_hid_rcu_keyboard->kind);
        printf("%s recv_usb_hid_rcu_keyboard.special_key = [%d]\r\n", SLE_RCU_DONGLE_LOG,
                    recv_usb_hid_rcu_keyboard->special_key);
        printf("%s recv_usb_hid_rcu_keyboard.reversed = [%d]\r\n", SLE_RCU_DONGLE_LOG,
                    recv_usb_hid_rcu_keyboard->reserve);
        printf("%s recv_usb_hid_rcu_keyboard.key = ", SLE_RCU_DONGLE_LOG);
        for (uint8_t i = 0; i < USB_HID_RCU_MAX_KEY_LENTH; i++) {
            printf("0x%02x ", recv_usb_hid_rcu_keyboard->key[i]);
        }
		if(recv_usb_hid_rcu_keyboard->key[0] != 0) {
				key_code = key_code_switch(recv_usb_hid_rcu_keyboard->key[0]);
				key_value = 1;
		} else {
				key_value = 0;
		}
        printf("\r\n");
		memset(data_to_send,0,sizeof(char)*30);
		snprintf(data_to_send, 30, "code:%d,value:%d\r\n", key_code,key_value);
		send_data_to_serial(data_to_send);
    } else {
		printf("[%s]this is not keyboard data\r\n",__func__);
	}
}

static void sle_rcu_indication_cb(uint8_t client_id, uint16_t conn_id, ssapc_handle_value_t *data, errcode_t status)
{
    if (data == NULL || data->data_len == 0 || data->data == NULL) {
        printf("%s sle_rcu_notification_cb fail, recv data is null!\r\n", SLE_RCU_DONGLE_LOG);
    }
    if (data->data_len == USB_RCU_KEYBOARD_REPORTER_LEN) {
        usb_hid_rcu_keyboard_report_t *recv_usb_hid_rcu_keyboard = NULL;
        printf("%s sle rcu recive notification\r\n", SLE_RCU_DONGLE_LOG);
        recv_usb_hid_rcu_keyboard = (usb_hid_rcu_keyboard_report_t *)data->data;
        printf("%s recv_usb_hid_rcu_keyboard.kind = [%d]\r\n", SLE_RCU_DONGLE_LOG,
                    recv_usb_hid_rcu_keyboard->kind);
        printf("%s recv_usb_hid_rcu_keyboard.special_key = [%d]\r\n", SLE_RCU_DONGLE_LOG,
                    recv_usb_hid_rcu_keyboard->special_key);
        printf("%s recv_usb_hid_rcu_keyboard.reversed = [%d]\r\n", SLE_RCU_DONGLE_LOG,
                    recv_usb_hid_rcu_keyboard->reserve);
        printf("%s recv_usb_hid_rcu_keyboard.key = ", SLE_RCU_DONGLE_LOG);
        for (uint8_t i = 0; i < USB_HID_RCU_MAX_KEY_LENTH; i++) {
            printf("0x%02x ", recv_usb_hid_rcu_keyboard->key[i]);
        }
        printf("\r\n");
    } else {
		printf("[%s]this is not keyboard data\r\n",__func__);
	}
}

void sle_sample_ssapc_cbk_register(ssapc_notification_callback notification_cb,
                                                     ssapc_notification_callback indication_cb)
{
    g_ssapc_cbk.exchange_info_cb = sle_sample_exchange_info_cbk;
    g_ssapc_cbk.find_structure_cb = sle_sample_find_structure_cbk;
    g_ssapc_cbk.find_structure_cmp_cb = sle_sample_find_structure_cmp_cbk;
    g_ssapc_cbk.ssapc_find_property_cbk = sle_sample_find_property_cbk;
    g_ssapc_cbk.notification_cb = notification_cb;
    g_ssapc_cbk.indication_cb = indication_cb;
    g_ssapc_cbk.write_cfm_cb = sle_sample_write_cfm_cbk;
    g_ssapc_cbk.read_cfm_cb = sle_sample_read_cfm_cbk;
}

void sle_client_init()
{
    sle_sample_seek_cbk_register();
    sle_sample_connect_cbk_register();
    sle_sample_ssapc_cbk_register(sle_rcu_notification_cb,sle_rcu_indication_cb);
    sle_announce_seek_register_callbacks(&g_seek_cbk);
    sle_connection_register_callbacks(&g_connect_cbk);
    ssapc_register_callbacks(&g_ssapc_cbk);
    enable_sle();

}

void sle_start_scan()
{
    sle_seek_param_t param = {0};
    param.own_addr_type = 0;
    param.filter_duplicates = 0;
    param.seek_filter_policy = 0;
    param.seek_phys = 1;
    param.seek_type[0] = 0;
    param.seek_interval[0] = SLE_SEEK_INTERVAL_DEFAULT;
    param.seek_window[0] = SLE_SEEK_WINDOW_DEFAULT;
    sle_set_seek_param(&param);
    sle_start_seek();
}
void sigintHandler(int sig)
{
    sig = sig;

    // 关闭连接
    errcode_t ret = sle_disconnect_remote_device(&g_remote_addr);
    printf("*****sle disconnect remote device(ret %u) ****\n\n",ret);
}

int main(int argc, char *argv[])
{
	int ret;
	ret = uapi_gle_init();
	if (ret != 0) {
		printf("init fail.\n\n");
		goto mainend;
	}
    // 创建信号处理函数
    signal(SIGINT, sigintHandler);
	// start业务功
	sle_client_init();
	// end业务功能
	while(1);
mainend:
	printf("main start. 00\n\n");
	printf("main end.\n\n");
	return ret;
}