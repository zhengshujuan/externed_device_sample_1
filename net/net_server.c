

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>

#define SERVER_PORT 12345
#define BUFFER_SIZE 1024

void error(const char *msg) {
    perror(msg);
    exit(1);
}

int main() {
    int sockfd, newsockfd;
    socklen_t clilen;
    struct sockaddr_in server_addr, client_addr;
    char buffer[BUFFER_SIZE];
    ssize_t bytes_received, total_bytes_received = 0;

    // 创建套接字
    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd < 0) {
        error("ERROR opening socket");
    }

    // 设置服务器地址
    memset((char *)&server_addr, 0, sizeof(server_addr));
    server_addr.sin_family = AF_INET;
    server_addr.sin_addr.s_addr = INADDR_ANY;
    server_addr.sin_port = htons(SERVER_PORT);

    // 绑定套接字到地址
    if (bind(sockfd, (struct sockaddr *)&server_addr, sizeof(server_addr)) < 0) {
        error("ERROR on binding");
    }

    // 监听套接字
    listen(sockfd, 5);

    printf("Server listening on port %d...\n", SERVER_PORT);

    // 接受连接
    clilen = sizeof(client_addr);
    newsockfd = accept(sockfd, (struct sockaddr *)&client_addr, &clilen);
    if (newsockfd < 0) {
        error("ERROR on accept");
    }

    // 循环接收数据
    while ((bytes_received = recv(newsockfd, buffer, BUFFER_SIZE, 0)) > 0) {
        total_bytes_received += bytes_received;
        // 在这里你可以对接收到的数据进行处理
    }

    if (bytes_received < 0) {
        error("ERROR reading from socket");
    }

    printf("Received %zd MB from the client.\n", (total_bytes_received)/(1024*1024));

    // 关闭套接字
    close(newsockfd);
    close(sockfd);

    return 0;
}

