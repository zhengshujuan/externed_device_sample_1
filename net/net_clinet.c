

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <time.h>

#define SERVER_IP "192.168.1.2"
#define SERVER_PORT 12345
#define BUFFER_SIZE 1024
#define DATA_SIZE (400 * 1024 * 1024) // 200MB

void error(const char *msg) {
    perror(msg);
    exit(1);
}

double get_elapsed_time(struct timespec start, struct timespec end) {
    return (end.tv_sec - start.tv_sec) + (end.tv_nsec - start.tv_nsec) / 1e9;
}

int main() {
    int sockfd;
    struct sockaddr_in server_addr;
    char buffer[BUFFER_SIZE];
    ssize_t bytes_sent, bytes_received;
    struct timespec start_time, end_time;
    double elapsed_time, download_speed;
    int total_bytes_sent = 0;
    //double may_time;
    // 创建套接字
    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd < 0) {
        error("ERROR opening socket");
    }

    // 设置服务器地址
    server_addr.sin_family = AF_INET;
    server_addr.sin_port = htons(SERVER_PORT);
    inet_aton(SERVER_IP, &server_addr.sin_addr);

    // 连接到服务器
    if (connect(sockfd, (struct sockaddr *)&server_addr, sizeof(server_addr)) < 0) {
        error("ERROR connecting");
    }

    // 准备测试数据
    memset(buffer, 'b', BUFFER_SIZE);

    // 开始计时
    clock_gettime(CLOCK_MONOTONIC, &start_time);

    // 循环发送数据到服务器，直到达到指定大小
    while (total_bytes_sent < DATA_SIZE) {
        // 发送数据到服务器
        bytes_sent = send(sockfd, buffer, BUFFER_SIZE, 0);
        if (bytes_sent < 0) {
            error("ERROR writing to socket");
        }
        total_bytes_sent += bytes_sent;
	//printf("total bytes is %d\n",total_bytes_sent);
    }
    //printf("jieshou\n");
    // 接收服务器的响应
   // bytes_received = recv(sockfd, buffer, BUFFER_SIZE, 0);

    // 停止计时
    clock_gettime(CLOCK_MONOTONIC, &end_time);
  // may_time = end_time -start_time;
  // printf("mayt_time is %.2f",may_time);
    // 计算下载速率（Mbps）
    elapsed_time = get_elapsed_time(start_time, end_time);
    download_speed = (total_bytes_sent * 8.0) / (1e6 * elapsed_time);

    printf("Download Speed: %.2f Mbps\n", download_speed);

    // 关闭套接字
    close(sockfd);

    return 0;
}

